# Service Usage

* GFZ-Sensor-Mgmt

## Plotting

* Plotting is be performed in the [Plotting project](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci).

## Data

| Data           | Unit            | Weighting | Comment                                                                    |
|----------------|-----------------|---------- | ---------------------------------------------------------------------------|
| configuration_pids | Quantity/Number | 0 | Count of configurations with PIDs |
| configurations | Quantity/Number | 10      | Count of configurations (aka measuring setups)                             |
| datastreams | Quantity/Number | 0 | Count of datastreams linked to the metadata in the SMS |
| device_pids | Quantity/Number | 0 | Count of devices with PIDs |
| devices        | Quantity/Number | 20      | Current count of devices in the system. Contains public, internal & private devices.|
| orcids | Quantity/Number | 0 | Number of contact entries that are linked with ORCIDs |
| organizations | Quantity/Number | 0 | Number of distinct organizations that the SMS users belong to. |
| pids | Quantity/Number | 0 | Current number of PIDs in the SMS. Sum of device_pids, configuration_pids and platform_pids |
| platform_pids | Quantity/Number | 0 | Current number of platforms with PIDs |
| platforms      | Quantity/Number | 15      | Current count of platforms. Contains public, internal & private platforms. |
| sites          | Quantity/Number | 5      | Count of sites (locations of measurements).                                                            |
| uploads | Quantity/Number | 0 | Count of uploaded attachment files in the SMS. |
| users          | Quantity/Number | 50,0 %    | Count of different users that logged into the system.                      |

* 50% users 
* 50% usage
  * configurations
  * devices
  * platforms
  * sites

## Schedule

* daily
